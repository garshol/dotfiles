#
# ~/.bashrc
#

alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano -w PKGBUILD'
alias more=less
#alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "native-path|state|to\ full|percentage" && upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep -E "native-path|state|to\ full|percentage"'

