set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'https://github.com/junegunn/fzf.vim'
Plugin 'https://github.com/itchyny/lightline.vim'
Plugin 'https://github.com/terryma/vim-multiple-cursors'
Plugin 'https://github.com/tpope/vim-eunuch'
Plugin 'https://github.com/tpope/vim-surround'
Plugin 'https://github.com/scrooloose/nerdtree'
Plugin 'https://github.com/mattn/emmet-vim'
Plugin 'https://github.com/w0rp/ale'
Plugin 'https://github.com/mzlogin/vim-markdown-toc'
Plugin 'https://github.com/ctrlpvim/ctrlp.vim'
Plugin 'https://github.com/vimwiki/vimwiki'
" ----------------------------------------------------------
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Basic sets
set nu
map <C-n> :NERDTreeToggle<CR>
set relativenumber
set laststatus=2
set noshowmode
set shell=/bin/bash
set go=a
set mouse=a
set nohlsearch
set clipboard=unnamedplus
set autoindent
set tabstop=4
set smartcase
set ignorecase
set mouse=a
set encoding=utf-8
" Sane splits compared to defaults
set splitright
set splitbelow
" Color Mode
set bg=dark
set t_Co=256

" Filetypes handling
filetype plugin indent on
syntax on

" Maximum map and key delays
set timeoutlen=500
set ttimeoutlen=0

" Map space, , and \ as leader keys
let mapleader=",\<Space>"
let maplocalleader="\\"

" Use <space><space> to toggle to the last buffer
nnoremap <leader><leader> <c-^>

" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" Luke Smiths .vimrc from https://github.com/LukeSmithxyz

" Shortcutting split navigation, saving a keypress:
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

" Replace all is aliased to S.
	nnoremap S :%s/_/g"_c4l

" Ensure files are read as what I want:
	let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
	let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
	autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
	autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
	autocmd BufRead,BufNewFile *.tex set filetype=tex

" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" Run xrdb whenever Xdefaults or Xresources are updated.
	autocmd BufWritePost *Xresources,*Xdefaults !xrdb %

"MARKDOWN
	autocmd Filetype markdown,rmd,md map <leader>w yiWi[<esc>Ea](<esc>pa)
	autocmd Filetype markdown,rmd,md inoremap ,n ---<Enter><Enter>
	autocmd Filetype markdown,rmd,md inoremap ,b ****<++><Esc>F*hi
	autocmd Filetype markdown,rmd,md inoremap ,s ~~~~<++><Esc>F~hi
	autocmd Filetype markdown,rmd,md inoremap ,e **<++><Esc>F*i
	autocmd Filetype markdown,rmd,md inoremap ,h ====<Space><++><Esc>F=hi
	autocmd Filetype markdown,rmd,md inoremap ,i ![](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd,md inoremap ,a [](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd,md inoremap ,1 #<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,2 ##<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,3 ###<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd,md inoremap ,l --------<Enter>
	autocmd Filetype rmd,md inoremap ,r ```{r}<CR>```<CR><CR><esc>2kO
	autocmd Filetype rmd,md inoremap ,p ```{python}<CR>```<CR><CR><esc>2kO
	autocmd Filetype rmd,md inoremap ,c ```<cr>```<cr><cr><esc>2kO

" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
" From Timvisees dotfiles: https://github.com/timvisee/dotfiles/
" Auto Expanding
inoremap (, (<CR>),<C-o>0
inoremap {, {<CR>},<C-o>0
inoremap [, [<CR>],<C-o>0

" Common Typos
nnoremap q: :q
cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev WQ wq

