# Make fish stop nagging about $TERM environment variable
set TERM xterm-256color
# Setting for the new UTF-8 terminal support + Perl
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
# Setting for the new UTF-8 terminal support in Lion
#LC_CTYPE=en_US.UTF-8
#LC_ALL=en_US.UTF-8


###########################################
# Alias
###########################################
# alias mkcd='_(){ mkdir $1; cd $1; }; _'
# Enable or disable Touchpad
alias dtp="sh /home/garshol/.config/fish/functions/disable-touchpad"
alias etp="sh ~/.config/fish/functions/enable-touchpad"
# Shortcut for Ttrizen Package manager
alias get="trizen"
#Have a hard time to remember baobab
alias diskfree="baobab"
# Exit
alias :q="exit"
# Always prompt before delete on Crontab
alias crontab='crontab -i'
# Always show progress during dd
alias dd='dd status=progress'
# Make rm interactive and verbose
alias rm='rm -I -v'
# Makes cp interactive
alias cp='cp -i'
# Always show human readable formats in df
alias df='df -h'
# Make PDF of markdown with Grip
alias pdf="grip"
# Show centered clock in terminal
alias klk="tty-clock -cC5"
# Refresh GPG Keys
alias gpgrefresh="sudo pacman-key --refresh-key"
# Denying myself from bad habits
alias sl="sl -alc"
# Print Battery Status
alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "native-path|state|to\ full|percentage" & upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep -E "native-path|state|to\ full|percentage"'
# Python
alias py="python3"
alias python="python3"
# Git
alias push="git push origin master"
alias pull="git pull origin master"
# Navigating
alias ..="cd .."
alias notes="cd ~/Sync/Notes/"
alias cdrive="cd ~/.wine/drive_c/"
# Package Cleanup
alias cleanup_isle_3="bash;sudo pacman -R 'pacman -Qdtq'"
# Download Music files with 356k bitrate with YTDL
alias music-dl="youtube-dl -x --audio-format mp3 --audio-quality 356k --yes-playlist"
# Easy update
alias update="trizen -Syu"
# Silent installations
alias getq="trizen -q"

################################################
# FUNCTIONS ####################################
################################################

# cd to your recently kreated directory
function mkcd
		mkdir -p $argv
		cd $argv
end


# Edit trackpoint speed and sensitvity to my liking, defined as a function
function point
	sudo thinkpad-tools trackpoint set-speed 97
	sudo thinkpad-tools trackpoint set-sensitivity 100
	dtp
end

# Setting title to current command and PWD
function fish_title
	echo $argv[1] ' '
	pwd
end
# Start Starfish prompt
starship init fish | source
