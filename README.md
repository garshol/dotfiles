My dotfiles are just a config i've been working on as i go about using i3, and configuring it to my liking.

These files have some required software to work properly:

## Software
- urxvt
- [Polybar](https://polybar.github.io/) (Bar replacement for i3-bar)
- [rofi](https://github.com/davatorium/rofi) (Launcher replacement for dmenu)
- [fish shell](https://fishshell.com/) (Replacement shell for BASH)
- [i3lock-fancy](https://github.com/meskarune/i3lock-fancy)
- Blueman
- nm-applet
- nitrogen / feh


## Fonts
- font-awesome-5
- siji

